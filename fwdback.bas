Option Explicit

' An Excel add-in that allows you to use Alt-RightArrow to go to the precedents
' of a particular cell and Alt-LeftArrow to go back.  This small feat can
' already be accomplished in Excel with "Ctrl-[" followed by "Ctrl-G-Enter".
' The value of this add-in is that it keeps track of your last 100 hops so that
' you can use "Alt-LeftArrow" multiple times to travel back through multiple
' formulas until you get back to the original source.  This is useful when
' links are daisy chained and you want to go to the original source then back
' to the formula you are investigating.

' Known limitations:
'   - Does not work with links to files that are not open.
'   - Clobbers the stack when more than 1 instance of Excel is running
'   - Surely more!

' % = Alt
' ^ = Control
' + = Shift
Private Const FORWARD_SHORTCUT = "%{RIGHT}"
Private Const BACKWRD_SHORTCUT = "%{LEFT}"

Private Const STACK_DEPTH = 100

Private Function get_stack_idx() As Long
  get_stack_idx = ThisWorkbook.Sheets("Tracking").Range("A1").Value
End Function

Private Function get_top_of_stack() As String
  Dim idx As Long
  idx = get_stack_idx()
  If idx = 0 Then
    get_top_of_stack = ""
  Else
    get_top_of_stack = ThisWorkbook.Sheets(1).Range("A" & idx)
  End If
End Function

Private Sub inc_stack_idx()
  Dim curr_stack_idx As Long
  curr_stack_idx = get_stack_idx()
  If curr_stack_idx < STACK_DEPTH Then
    ThisWorkbook.Sheets("Tracking").Range("A1").Value = curr_stack_idx + 1
  End If
End Sub

Private Sub dec_stack_idx()
  Dim curr_stack_idx As Long
  curr_stack_idx = get_stack_idx()
  If curr_stack_idx > 0 Then
    ThisWorkbook.Sheets("Tracking").Range("A1").Value = curr_stack_idx - 1
  End If
End Sub

Private Sub reset_stack_idx()
  ThisWorkbook.Sheets("Tracking").Range("A1").Value = 0
End Sub

Private Sub add_to_stack(rng As Range)
  Dim stack_ptr As Range
  Dim next_idx As Integer
  next_idx = get_stack_idx()
  If next_idx < STACK_DEPTH Then
    next_idx = next_idx + 1
  Else
    ThisWorkbook.Sheets(1).Range("A1:A" & STACK_DEPTH - 1).Value = _
      ThisWorkbook.Sheets(1).Range("A2:A" & STACK_DEPTH).Value
  End If
  Set stack_ptr = ThisWorkbook.Sheets(1).Range("A" & next_idx)
  ' Need 2 single quotes bc one will be swallowed by excel never to be
  ' seen again in this macro ...
  stack_ptr.Value = "'" & get_relative_address(stack_ptr, rng)
  Call inc_stack_idx()
End Sub

Private Sub go_forward()
  ' Go to the first precedent and store this cell on the stack so we can go
  ' back to it later if needed.
  Dim fp As String
  fp = find_first_precedent()
  If fp <> "" Then
    Call add_to_stack(ActiveCell)
    Application.Goto Range(find_first_precedent())
  End If
End Sub

Private Sub go_back()
  Dim prev_cell As String
  prev_cell = get_top_of_stack()
  If prev_cell = "" Then Exit Sub
  Call dec_stack_idx()
  Application.Goto Range(prev_cell)
End Sub

Private Function get_relative_address(src As Range, dest As Range) As String
  ' Returns a range address (string) for 'dest' relative to 'src'.  This can
  ' be used in gotos, etc.
  get_relative_address = ""
  If src.Worksheet.Parent.Name = dest.Worksheet.Parent.Name Then
    If src.Worksheet.Name = dest.Parent.Name Then
      ' local
      get_relative_address = Selection.Address
    Else
      get_relative_address = "'" & Selection.Parent.Name & "'!" & Selection.Address
    End If
  Else
    ' external
    get_relative_address = Selection.Address(external:=True)
  End If
End Function

' http://www.ozgrid.com/forum/showthread.php?t=17028
Private Function find_first_precedent() As String
  find_first_precedent = ""
  ' written by Bill Manville
  ' With edits from PaulS
  ' this procedure finds the cells which are the direct precedents of the active cell
  Dim rLast As Range, iLinkNum As Integer, iArrowNum As Integer
  Dim stMsg As String
  Dim bNewArrow As Boolean
  Application.ScreenUpdating = False
  ActiveCell.ShowPrecedents
  Set rLast = ActiveCell
  iArrowNum = 1
  iLinkNum = 1
  bNewArrow = True
  Do
    Do
      Application.Goto rLast
      On Error Resume Next
      ActiveCell.NavigateArrow TowardPrecedent:=True, ArrowNumber:=iArrowNum, LinkNumber:=iLinkNum
      If Err.Number > 0 Then Exit Do
      On Error GoTo 0
      If rLast.Address(external:=True) = ActiveCell.Address(external:=True) Then Exit Do
      bNewArrow = False
      stMsg = get_relative_address(rLast, ActiveCell)
      ' Only need the 1st precedent (for now)
      Goto finish_up
      iLinkNum = iLinkNum + 1  ' try another link
    Loop
    If bNewArrow Then Exit Do
    iLinkNum = 1
    bNewArrow = True
    iArrowNum = iArrowNum + 1  'try another arrow
  Loop
finish_up:
  rLast.Parent.ClearArrows
  Application.Goto rLast
  find_first_precedent = stMsg
End Function

Private Sub set_macro_keys()
  Application.OnKey FORWARD_SHORTCUT, "go_forward"
  Application.OnKey BACKWRD_SHORTCUT, "go_back"
End Sub

Private Sub unset_macro_keys()
  Application.OnKey FORWARD_SHORTCUT
  Application.OnKey BACKWRD_SHORTCUT
End Sub

Sub Auto_Open()
  Call set_macro_keys()
  Call reset_stack_idx()
End Sub
