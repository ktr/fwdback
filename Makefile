
DEBUG = 1
MAJOR_VERSION = 0
MINOR_VERSION = 0
PATCH_VERSION = 2
VERSION = $(MAJOR_VERSION).$(MINOR_VERSION).$(PATCH_VERSION)

# If you installed Lua somewhere else, change the next 3 lines
# to match your setup.
LUA_DIR = C:/PROGRA~2/Lua/5.1
LUA_INC = $(LUA_DIR)/include/
LUA = $(LUA_DIR)/lua.exe

# NSIS creates the actual installer.
NSIS = C:/PROGRA~2/NSIS/makensis.exe

INCLUDES  = -I$(LUA_INC)
INCLUDES += -I.

CFLAGS = -std=c99 $(INCLUDES)

INSTALLER_NAME = fdb-$(VERSION).exe

$(INSTALLER_NAME): installer.nsi LICENSE fwdback.xla
	sed -i "s/!Define VERSION.*/!Define VERSION $(VERSION)/" installer.nsi
	$(NSIS) installer.nsi

installer: $(INSTALLER_NAME)

ifndef DEBUG
    LUA_OPTS = "-seu "
endif

clean:
	$(RM) $(INSTALLER_NAME)
	$(RM) fdb*.exe

.PHONY: installer clean
