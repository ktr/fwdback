!Include LogicLib.nsh
!Include MUI2.nsh

; include for some of the windows messages defines (for setting env vars)
!Include "winmessages.nsh"

!Define VERSION 0.0.2
OutFile "fdb-${VERSION}.exe"

!Define ADDIN_DIR $APPDATA\Microsoft\AddIns
!Define ADDIN_NAM "Fwdback"

InstallDir "$PROGRAMFILES\fdb"

!InsertMacro MUI_PAGE_LICENSE "LICENSE"
!InsertMacro MUI_PAGE_INSTFILES
!InsertMacro MUI_LANGUAGE "English"

# default section start
Section
  ; We need to install the XLS file into the Addin
  ; directory so that we can auto-load it.
  SetOutPath ${ADDIN_DIR}
  File fwdback.xla

  ; Rather then trying to load with VBA, we'll use
  ; LuaCom for this purpose.
  SetOutPath $INSTDIR
  File addin_load.exe
  File LICENSE
  File "C:\Program Files (x86)\Lua\5.1\clibs\luacom.dll"
  File "C:\Program Files (x86)\Lua\5.1\lib\lua5.1.dll"
  nsExec::ExecToLog '$INSTDIR\addin_load.exe "${ADDIN_NAM}" install'

  ; if they decide to uninstall
  WriteUninstaller $INSTDIR\uninstall-fdb.exe
SectionEnd

Section "Uninstall"
  # Make sure we can successfully uninstall from Excel before
  # doing anything else.
  nsExec::ExecToStack '$INSTDIR\addin_load.exe "${ADDIN_NAM}" uninstall'
  Pop $0
  ${If} $0 = 0
    # Always delete uninstaller first
    Delete $INSTDIR\uninstall-fdb.exe
    Delete $INSTDIR\addin_load.exe
    Delete $INSTDIR\luacom.dll
    Delete $INSTDIR\LICENSE
    Delete $INSTDIR\lua5.1.dll
    Delete /REBOOTOK ${ADDIN_DIR}\fwdback.xla
    RmDir  $INSTDIR
  ${Else}
    Pop $0
    MessageBox mb_ok "$0"
  ${EndIf}
SectionEnd
