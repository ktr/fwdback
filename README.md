
Example usage:

    +------+ <- Index starts at 0
    |      |
    +------+

    Adding to the stack:
    +------+
    | DATA |
    +------+ <- Index moves to 1

    *--------------------------------------------------------*
     Example movements:
    *--------------------------------------------------------*
    Go forward 3 times (e.g., following a formula):
    +------+
    | DATA |
    +------+
    | DATA |
    +------+
    | DATA |
    +------+ <- Index at 3

    Go back:
    +------+
    | DATA |
    +------+
    | DATA |
    +------+ <- Index at 2
    | DATA |
    +------+
    To go forward again along the same path, need to move the
    pointer forward one spot and then jump to that location.
